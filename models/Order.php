<?php
namespace webdevgroup\bidfoxPartnerApi\models;


class Order
{
    const STATUS_NEW = 3;
    const STATUS_APPROVED = 4;
    const STATUS_CANCELLED = 5;
    
    /** @var string */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $country;

    /** @var string */
    public $phone;

    /** @var int */
    public $tz;

    /** @var int */
    public $offer_id;

    /** @var string */
    public $address;

    /** @var string */
    public $utm_source;

    /** @var string */
    public $utm_medium;

    /** @var string */
    public $utm_campaign;

    /** @var string */
    public $utm_term;

    /** @var string */
    public $utm_content;

    /** @var string */
    public $bidfox_id;

    /** @var float */
    public $price;

    /** @var float */
    public $payment;

    /** @var string */
    public $status;
    
    public function toArray()
    {
        return array_filter([
            'external_id'  => $this->id,
            'name'         => $this->name,
            'country'      => $this->country,
            'phone'        => $this->phone,
            'tz'           => $this->tz,
            'offer_id'     => $this->offer_id,
            'address'      => $this->address,
            'utm_source'   => $this->utm_source,
            'utm_medium'   => $this->utm_medium,
            'utm_campaign' => $this->utm_campaign,
            'utm_term'     => $this->utm_term,
            'utm_content'  => $this->utm_content,
            'own_id'       => $this->bidfox_id,
            'price'        => $this->price,
            'payment'      => $this->payment,
            'status'       => $this->status,
        ]);
    }
    
    public function setAttributes(array $attributes)
    {
        $this->id           = isset($attributes['external_id']) ? $attributes['external_id'] : $this->id;
        $this->name         = isset($attributes['name']) ? $attributes['name'] : $this->name;
        $this->country      = isset($attributes['country']) ? $attributes['country'] : $this->country;
        $this->phone        = isset($attributes['phone']) ? $attributes['phone'] : $this->phone;
        $this->tz           = isset($attributes['tz']) ? $attributes['tz'] : $this->tz;
        $this->offer_id     = isset($attributes['offer_id']) ? $attributes['offer_id'] : $this->offer_id;
        $this->address      = isset($attributes['address']) ? $attributes['address'] : $this->address;
        $this->utm_source   = isset($attributes['utm_source']) ? $attributes['utm_source'] : $this->utm_source;
        $this->utm_medium   = isset($attributes['utm_medium']) ? $attributes['utm_medium'] : $this->utm_medium;
        $this->utm_campaign = isset($attributes['utm_campaign']) ? $attributes['utm_campaign'] : $this->utm_campaign;
        $this->utm_term     = isset($attributes['utm_term']) ? $attributes['utm_term'] : $this->utm_term;
        $this->utm_content  = isset($attributes['utm_content']) ? $attributes['utm_content'] : $this->utm_content;
        $this->bidfox_id    = isset($attributes['own_id']) ? $attributes['own_id'] : $this->bidfox_id;
        $this->price        = isset($attributes['price']) ? $attributes['price'] : $this->price;
        $this->payment      = isset($attributes['payment']) ? $attributes['payment'] : $this->payment;
        $this->status       = isset($attributes['status']) ? $attributes['status'] : $this->status;
    }
}