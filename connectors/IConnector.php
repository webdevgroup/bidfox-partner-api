<?php
namespace webdevgroup\bidfoxPartnerApi\connectors;


interface IConnector
{
    public function call($method, array $params);
    
    public function processRequest($data);
}