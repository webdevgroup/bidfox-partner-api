<?php
namespace webdevgroup\bidfoxPartnerApi\connectors;


use webdevgroup\bidfoxPartnerApi\exceptions\ApiNotAvailableException;
use webdevgroup\bidfoxPartnerApi\exceptions\ApiRequestFailedException;
use webdevgroup\bidfoxPartnerApi\exceptions\InvalidConfigException;
use webdevgroup\bidfoxPartnerApi\exceptions\WrongFormatException;
use webdevgroup\bidfoxPartnerApi\exceptions\WrongSignatureException;


class Connector implements IConnector
{
    /** @var string */
    public $url = 'http://postbar.ru/gateway/universal/index';
 
    /** @var string */
    public $secret;

    public $request_timeout;

    public function call($method, array $params)
    {
        if( !($curl = curl_init() )) {
            throw new InvalidConfigException('CURL extension should be enabled');
        }

        $data = json_encode($this->processParams($method, $params));

        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        curl_setopt($curl, CURLOPT_TIMEOUT, ceil($this->request_timeout / 1000));


        $response = curl_exec($curl);

        curl_close($curl);

        if(!$response){
            throw new ApiNotAvailableException();
        }

        if(!($result = json_decode($response, true))){
            throw new WrongFormatException(
                'Response has wrong format ' . print_r($response, true) . '; ' . json_last_error_msg(),
                json_last_error()
            );
        }

        if(!isset($result['result'])){
            throw new WrongFormatException('Response has no result field');
        }

        if($result['result']){
            return isset($result['data']) ? $result['data'] : [];
        }
        throw new ApiRequestFailedException($result['error_message'], $result['error_code']);
    }

    private function processParams($method, array $params)
    {
        return [
            'action'    => $method,
            'data'      => $params,
            'date'      => $date = time(),
            'signature' => $this->generateSignature($method, $params, $date),
        ];
    }

    private function sortParams(array $params)
    {
        $params = array_map(function(array $param){
            return is_array($param) ? $this->sortParams($param) : $param;
        }, $params);

        ksort($params);

        return $params;
    }

    private function generateSignature($action, array $params, $date)
    {
        return md5(implode('|', [
            $action,
            $date,
            $this->flatParams($params),
            $this->secret,
        ]));
    }

    /**
     * @param array $params
     *
     * @return string
     */
    private function flatParams(array $params)
    {
        ksort($params);

        return implode('|', array_map(function($item){

            return is_array($item) ? $this->flatParams($item) : $item;

        }, $params));
    }
    
    public function processRequest($data)
    {
        if(!($request = json_decode($data, true))){
            throw new WrongFormatException(
                'Response has wrong format ' . print_r($data, true) . '; ' . json_last_error_msg(),
                json_last_error()
            );
        }
        
        if(!isset($request['signature'])) {
            throw new WrongFormatException('Request has no signature field');
        }

        if(!isset($request['action'])) {
            throw new WrongFormatException('Request has no action field');
        }

        if(!isset($request['date'])) {
            throw new WrongFormatException('Request has no date field');
        }

        if(!isset($request['data'])) {
            throw new WrongFormatException('Request has no data field');
        }

        if($this->generateSignature($request['action'], $request['data'], $request['date']) != $request['signature']) {

            throw new WrongSignatureException('Wrong signature');
        }

        return [$request['action'], $request['data']];
    }
}