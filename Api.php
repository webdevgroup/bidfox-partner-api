<?php
namespace webdevgroup\bidfoxPartnerApi;


use webdevgroup\bidfoxPartnerApi\connectors\IConnector;
use webdevgroup\bidfoxPartnerApi\exceptions\ApiException;
use webdevgroup\bidfoxPartnerApi\models\Order;
use webdevgroup\bidfoxPartnerApi\models\Request;

class Api
{
    /** @var IConnector */
    private $_connector;
    
    public function __construct(IConnector $connector)
    {
        $this->_connector = $connector;
    }
    
    public function postback(Order $order)
    {
        $this->_connector->call('order.postback', $order->toArray());
    }

    /**
     * @param string $data
     *
     * @return Request
     *
     * @throws ApiException
     */
    public function processRequest($data)
    {
        list($action, $request) = $this->_connector->processRequest($data);

        switch ($action) {

            case 'order.create':
                return $this->processOrderCreateData($request);
            case 'order.index':
                return $this->processOrderIndexData($request);
            default:
                throw new ApiException('Unknown API call: ' . $action);
        }
    }

    private function processOrderCreateData($data)
    {
        $request = new Request();

        $request->data = new Order();

        $request->data->setAttributes($data);

        return $request;
    }

    private function processOrderIndexData($data)
    {
        $request = new Request();

        $request->data = $data;

        return $request;
    }
}