<?php


namespace webdevgroup\bidfoxPartnerApi\exceptions;


use Exception;

class ApiRequestFailedException extends ApiException
{

    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
} 